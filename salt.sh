echo ""
echo "################################### EXPORTING VARIABLES ###########################################"
source /root/variables.sh
echo ""
echo "################################### ALL VARIABLES EXPORTED ########################################"
echo ""

echo "###################################### EDITING HOSTS FILES ########################################"

echo "$cloud_ip $cloud_host $cloud_name" >> /etc/hosts
echo "$database_ip $db_host $db_name" >> /etc/hosts
echo ""
echo ""

echo "==================================================================================================="
echo ""
echo "################################### SALT MASTER CONFIFGS ##########################################"
echo ""
echo "==================================================================================================="
echo ""
sleep 5

sed -i '106s/172.28.2.69/'$database_ip'/g' /var/lib/enclouden-orchestrator/config/system.yaml
sed -i '115s/172.28.2.70/'$cloud_ip'/g' /var/lib/enclouden-orchestrator/config/system.yaml
sed -i '137s/172.28.2.71/'$server1'/g' /var/lib/enclouden-orchestrator/config/system.yaml
sed -i '138s/172.28.2.72/'$server2'/g' /var/lib/enclouden-orchestrator/config/system.yaml
sed -i '139s/172.28.2.73/'$server3'/g' /var/lib/enclouden-orchestrator/config/system.yaml
sed -i '9s/P069CKXGNJIX/'$salt_broker_pass'/g' /var/lib/enclouden-orchestrator/config/passwords.yaml
sed -i '10s/O9FX3H9M0ZMZ/'$salt_database_pass'/g' /var/lib/enclouden-orchestrator/config/passwords.yaml
sed -i '11s/KQGUYQTYZTPR/'$keystone_pass'/g' /var/lib/enclouden-orchestrator/config/passwords.yaml
cd /var/lib/enclouden-orchestrator
export FLASK_CONFIG=production_config
python manage.py deploy
echo "$salt_ip $salt_host" >> /etc/hosts
chmod 755 /var/log/enclouden/enclouden_orchestrator_production_config.log
chown apache:apache /var/log/enclouden/enclouden_orchestrator_production_config.log
systemctl enable salt-master
systemctl start salt-master
systemctl enable redis
systemctl start redis
systemctl enable httpd
systemctl start httpd

echo ""
echo "----------------------------------------------------------------------------------------------------"
echo ""
echo "###################################### ALL CONFIGS CHANGED #########################################"
echo ""
echo "----------------------------------------------------------------------------------------------------"
echo ""
echo ""

echo "##################################### EXTENDING ROOT PARTITON ######################################"
echo ""
growpart /dev/sda 2
pvresize /dev/sda2
lvextend -l +100%FREE /dev/centos/root
xfs_growfs /dev/centos/root
df -h
echo ""
echo "#################################### ROOT PARTITION EXTENDED ########################################"
