echo "##################################### EXTENDING ROOT PARTITON ######################################"
echo ""
growpart /dev/sda 2
pvresize /dev/sda2
lvextend -l +100%FREE /dev/centos/root
xfs_growfs /dev/centos/root
df -h
echo ""
echo "#################################### ROOT PARTITION EXTENDED ########################################"

cd /root/zookeeper-3.4.10

./bin/zkServer.sh start

sleep 5

./bin/zkServer.sh status

tmux new -s test -d
tmux new -s coordinator -d
tmux send-keys -t coordinator "cd /root/enclouden-spyagent/" ENTER
tmux send-keys -t coordinator "python manage.py run_druid_service --service_name coordinator" ENTER
sleep 3


tmux new -s overlord -d
tmux send-keys -t overlord "cd /root/enclouden-spyagent/" ENTER
tmux send-keys -t overlord "python manage.py run_druid_service --service_name overlord" ENTER

