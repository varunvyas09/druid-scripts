echo "##################################### EXTENDING ROOT PARTITON ######################################"
echo ""
growpart /dev/sda 2
pvresize /dev/sda2
lvextend -l +100%FREE /dev/centos/root
xfs_growfs /dev/centos/root
df -h
echo ""
echo "#################################### ROOT PARTITION EXTENDED ########################################"

tmux new -s test -d
tmux new -s historical -d
tmux send-keys -t historical "cd /root/enclouden-spyagent/" ENTER
tmux send-keys -t historical "python manage.py run_druid_service --service_name historical" ENTER

tmux new -s middle-manager -d
tmux send-keys -t middle-manager "cd /root/enclouden-spyagent/" ENTER
tmux send-keys -t middle-manager "python manage.py run_druid_service --service_name middle_manager" ENTER
sleep 20

tmux new -s tranqulity -d
tmux send-keys -t tranqulity "cd /root/tranquility-distribution-0.8.2/" ENTER
tmux send-keys -t tranqulity "bin/tranquility server -configFile spy_agent_server.json" ENTER
