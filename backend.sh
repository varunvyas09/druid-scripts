#!/bin/bash -x
echo ""
echo "################################### EXPORTING VARIABLES ###########################################"
source /root/variables.sh
echo ""
echo "################################### ALL VARIABLES EXPORTED ########################################"
echo ""

echo "==================================================================================================="
echo ""
echo "################################### CHANGING BACKEND VM CONFIFGS ##################################"
echo ""
echo "==================================================================================================="
echo ""
sleep 5

echo "################################ DRUID BACKEND VM CONFIGS CHANGED #################################"
echo ""

sed -i '8s/Q8Q2DLWQRZE7/'$broker_pass'/g' /var/lib/enclouden-spyagent/config/passwords.yaml
sed -i '9s/AGOILHEX9SFV/'$database_pass'/g' /var/lib/enclouden-spyagent/config/passwords.yaml
sed -i '10s/KQGUYQTYZTPR/'$keystone_pass'/g' /var/lib/enclouden-spyagent/config/passwords.yaml

sed -i '88s/172.28.2.69/'$database_ip'/g' /var/lib/enclouden-spyagent/config/system.yaml
sed -i '95s/172.28.2.70/'$cloud_ip'/g' /var/lib/enclouden-spyagent/config/system.yaml
sed -i '119s/172.28.2.71/'$server1'/g' /var/lib/enclouden-spyagent/config/system.yaml
sed -i '120s/172.28.2.72/'$server2'/g' /var/lib/enclouden-spyagent/config/system.yaml
sed -i '121s/172.28.2.73/'$server3'/g' /var/lib/enclouden-spyagent/config/system.yaml

sed -i '45s/192.168.207.173/'$backend'/g' /var/lib/enclouden-spyagent/config/system.yaml
sed -i '49s/192.168.207.14/'$historical'/g' /var/lib/enclouden-spyagent/config/system.yaml
sed -i '57s/192.168.207.173/'$backend'/g' /var/lib/enclouden-spyagent/config/system.yaml

echo "###################################### EDITING HOSTS FILES ########################################"

echo "$cloud_ip $cloud_host $cloud_name" >> /etc/hosts
echo "$database_ip $db_host $db_name" >> /etc/hosts
echo ""
echo "##################################### UPDATING THE DATABASE #######################################"
echo ""
cd /var/lib/enclouden-spyagent/
export FLASK_CONFIG=production_config
python manage.py deploy
chown apache:apache /var/log/enclouden/enclouden_spyagent_production_config.log
chmod 755 /var/log/enclouden/enclouden_spyagent_production_config.log
apachectl graceful
yes | cp -rf /var/lib/enclouden-spyagent/config/* /root/enclouden-spyagent/config/
echo ""
echo ""
echo ""

echo "==================================================================================================="
echo ""
echo "################################### DRUID CLUSTER VMS CONFIFGS ####################################"
echo ""
echo "==================================================================================================="
echo ""

sed -i '44s/localhost/'$zookeeper'/g' /root/druid-0.12.3/conf-quickstart/druid/_common/common.runtime.properties

sed -i '69d' /root/druid-0.12.3/conf-quickstart/druid/_common/common.runtime.properties
sed -i '69idruid.metadata.storage.connector.password='$druid'' /root/druid-0.12.3/conf-quickstart/druid/_common/common.runtime.properties
sed -i '67s/db.example.com/'$database_ip'/g' /root/druid-0.12.3/conf-quickstart/druid/_common/common.runtime.properties

sed -i '3idruid.host='$zookeeper'' /root/druid-0.12.3/conf-quickstart/druid/coordinator/runtime.properties

sed -i '3idruid.host='$zookeeper'' /root/druid-0.12.3/conf-quickstart/druid/overlord/runtime.properties

sed -i '3idruid.host='$broker'' /root/druid-0.12.3/conf-quickstart/druid/broker/runtime.properties

sed -i '3idruid.host='$historical'' /root/druid-0.12.3/conf-quickstart/druid/historical/runtime.properties

sed -i '3idruid.host='$historical'' /root/druid-0.12.3/conf-quickstart/druid/middleManager/runtime.properties

sed -i '80s/192.168.207.39/'$zookeeper'/g' /root/tranquility-distribution-0.8.2/spy_agent_server.json
echo ""
echo ""
echo "################################ DRUID CLUSTER VM CONFIGS CHANGED #################################"
echo ""
echo ""

echo "==================================================================================================="
echo ""
echo "################################### DRUID AGENT CONFIFGS ##########################################"
echo ""
echo "==================================================================================================="
echo ""

sed -i '3s/10.11.12.167/'$backend'/g' /root/ecnspy/agentconfig/settings.yaml
sed -i '3s/10.11.12.167/'$backend'/g' /root/ecnspy/agentconfig/static_settings.yaml

echo "==================================================================================================="
echo ""
echo "################################### SALT MASTER CONFIFGS ##########################################"
echo ""
echo "==================================================================================================="
echo ""

sed -i '106s/172.28.2.69/'$database_ip'/g' /var/lib/enclouden-orchestrator/config/system.yaml
sed -i '115s/172.28.2.70/'$cloud_ip'/g' /var/lib/enclouden-orchestrator/config/system.yaml
sed -i '137s/172.28.2.71/'$server1'/g' /var/lib/enclouden-orchestrator/config/system.yaml
sed -i '138s/172.28.2.72/'$server2'/g' /var/lib/enclouden-orchestrator/config/system.yaml
sed -i '139s/172.28.2.73/'$server3'/g' /var/lib/enclouden-orchestrator/config/system.yaml
sed -i '9s/P069CKXGNJIX/'$salt_broker_pass'/g' /var/lib/enclouden-orchestrator/config/passwords.yaml
sed -i '10s/O9FX3H9M0ZMZ/'$salt_database_pass'/g' /var/lib/enclouden-orchestrator/config/passwords.yaml
sed -i '11s/KQGUYQTYZTPR/'$keystone_pass'/g' /var/lib/enclouden-orchestrator/config/passwords.yaml
#cd /var/lib/enclouden-orchestrator
#export FLASK_CONFIG=production_config
#python manage.py deploy
echo "$salt_ip $salt_host" >> /etc/hosts

echo ""
echo "----------------------------------------------------------------------------------------------------"
echo ""
echo "###################################### ALL CONFIGS CHANGED #########################################"
echo ""
echo "----------------------------------------------------------------------------------------------------"
echo ""
echo ""

echo "##################################### EXTENDING ROOT PARTITON ######################################"
echo ""
growpart /dev/sda 2
pvresize /dev/sda2
lvextend -l +100%FREE /dev/centos/root
xfs_growfs /dev/centos/root
df -h
echo ""
echo "#################################### ROOT PARTITION EXTENDED ########################################"
echo ""
sleep 20
echo "##################################### TRANFERRING FOLDERS TO ZOOKEEPEER VM ##########################i"
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$zookeeper 'rm -rf /root/enclouden-spyagent'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$zookeeper 'rm -rf /root/druid-0.12.3'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$zookeeper 'rm -rf /root/tranquility-distribution-0.8.2'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$zookeeper 'rm -rf /root/zookeeper-3.4.10'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$zookeeper 'rm -rf /root/ecnspy'
sshpass -p 'server1011q2w' scp -o StrictHostKeyChecking=no -r /root/druid-0.12.3 root@$zookeeper:/root/
sshpass -p 'server1011q2w' scp -o StrictHostKeyChecking=no -r /root/zookeeper-3.4.10 root@$zookeeper:/root/
echo "druid"
sshpass -p 'server1011q2w' scp -o StrictHostKeyChecking=no -r /root/enclouden-spyagent root@$zookeeper:/root/
echo "scp spyagent"
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$zookeeper 'systemctl stop httpd'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$zookeeper 'systemctl disable httpd'


echo "##################################### TRANFERRING FOLDERS TO broker VM ##########################i"
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$broker 'rm -rf /root/enclouden-spyagent'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$broker 'rm -rf /root/druid-0.12.3'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$broker 'rm -rf /root/tranquility-distribution-0.8.2'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$broker 'rm -rf /root/zookeeper-3.4.10'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$broker 'rm -rf /root/ecnspy'
sshpass -p 'server1011q2w' scp -o StrictHostKeyChecking=no -r /root/druid-0.12.3 root@$broker:/root/
echo "druid"
sshpass -p 'server1011q2w' scp -o StrictHostKeyChecking=no -r /root/enclouden-spyagent root@$broker:/root/
echo "scp spyagent"
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$broker 'systemctl stop httpd'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$broker 'systemctl disable httpd'


echo "##################################### TRANFERRING FOLDERS TO historical VM ##########################"
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$historical 'rm -rf /root/enclouden-spyagent'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$historical 'rm -rf /root/druid-0.12.3'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$historical 'rm -rf /root/tranquility-distribution-0.8.2'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$historical 'rm -rf /root/zookeeper-3.4.10'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$historical 'rm -rf /root/ecnspy'
sshpass -p 'server1011q2w' scp -o StrictHostKeyChecking=no -r /root/druid-0.12.3 root@$historical:/root/
echo "druid"
sshpass -p 'server1011q2w' scp -o StrictHostKeyChecking=no -r /root/enclouden-spyagent  root@$historical:/root/
echo "scp spyagent"
sshpass -p 'server1011q2w' scp -o StrictHostKeyChecking=no -r /root/tranquility-distribution-0.8.2 root@$historical:/root/
echo "tranquility"
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$historical 'systemctl stop httpd'
sshpass -p 'server1011q2w' ssh -o StrictHostKeyChecking=no root@$historical 'systemctl disable httpd'
echo ""
echo ""

sleep 10

###############################################################################################################

tmux new -s test -d
tmux new -s backend -d
tmux send-keys -t backend "cd /var/lib/enclouden-spyagent/" ENTER
tmux send-keys -t backend "export FLASK_CONFIG=production_config" ENTER
tmux send-keys -t backend "python manage.py run_celery --beat" ENTER
