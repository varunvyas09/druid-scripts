#!/bin/bash
cd /var/lib/enclouden-sysadmin-dashboard/
export FLASK_CONFIG=production_config
python /var/lib/enclouden-sysadmin-dashboard/manage.py shell
Config.insert_initial_config()
