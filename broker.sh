echo "##################################### EXTENDING ROOT PARTITON ######################################"
echo ""
growpart /dev/sda 2
pvresize /dev/sda2
lvextend -l +100%FREE /dev/centos/root
xfs_growfs /dev/centos/root
echo ""
echo "#################################### ROOT PARTITION EXTENDED ########################################"

tmux new -s test -d
tmux new -s broker -d
tmux send-keys -t broker "cd /root/enclouden-spyagent/" ENTER
tmux send-keys -t broker "python manage.py run_druid_service --service_name broker" ENTER
